import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

  void main() {
  runApp(MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}

class _State extends State<MyApp> {
  Map _countries = new Map();

  void _getData() async {
    String url = "http://country.io/names.json";
    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      setState(() => _countries = json.decode(response.body));
      print("Loaded ${_countries.length} countries.");
    }
  }

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("MyApp")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Text("Countries"),
              new Expanded(child: new ListView.builder(
                itemCount: _countries.length,
                itemBuilder: (BuildContext context, int index) {
                  String key = _countries.keys.elementAt(index);
                  return new Row(
                    children: <Widget>[
                      new Text('$key: '),
                      new Text(_countries[key]),
                    ],
                  );
                },
              ))
            ],
          ),
        )
      ),
    );
  }
}