import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}

class _State extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("MyApp")
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new Text("Hello, World!"),
              new Expanded(child: new Image.asset("images/flutter.png")),
              new Expanded(child: new Image.network("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png")),
              new Expanded(child: new Image.network("https://www.gstatic.com/devrel-devsite/prod/v73fbca10ce7899da426d451b3f74ee09bc6846fcf427552c7e8e85261505ef2c/firebase/images/lockup.png")),
              new Expanded(child: new Image.network("https://angular.io/assets/images/logos/angular/logo-nav@2x.png")),
            ],
          ),
        )
      ),
    );
  }
}